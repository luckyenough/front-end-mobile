var langRootpath = './';
function toggleLang(hide) {
    var $li = $('.my-list');
    var isHide = $li.is(':hidden');
    if (hide) {
        $li.hide();
        return;
    }
    if (isHide) {
        $li.show();
    } else {
        $li.hide();
    }

}

function updateText(t) {
    t = t.currentTarget.value;
    var langList = ['en', 'zh', 'hk', 'jp'];
    if (/[a-z]{2}/.test(t)) {
        t = langList.includes(t) ? t : 'en'
        setLang(t);
        //     location.href = location.href.replace(reg1, '/mobile/'+t+'/');
    }
}
function handleSelectLang(event) {
    var $target = $(event.target);
    var val = $target.attr('data-value');
    $('.langSwitch1 option[value=' + val + ']').click();
    $('.langSwitch1 option[value=' + val + ']')[0].selected = true;
    $('.langSwitch1').trigger('change');
    $('.my-list').hide();
    $('.flag img').attr('src', langRootpath + 'h160/' + val + '.webp');
    localStorage.setItem('funLanguage', val)
    $('.langSwitch1').next('.nice-select').find('.current').text(val.toUpperCase());
}
$(function () {
    $('body').click(function (event) {
        $e = $(event.target);
        $p = $e.parents();
        var clickLang = false;
        $p.each(function (i, e) {
            if ($(e).hasClass('language__wrap')) {
                clickLang = true;
            }
        });
        if (clickLang) {
            toggleLang();
        } else {
            toggleLang(true)
        }
    });
    $('.langSwitch1').on('change', updateText);
    // $('.langSwitch1').next('.nice-select').click(toggleLang);
    $('.my-list li').click(handleSelectLang);
    let funLanguage = localStorage.getItem('funLanguage') || 'en';
    // $('.langSwitch1 option[value=' + funLanguage + ']').click();
    // $('.langSwitch1 option[value=' + funLanguage + ']')[0].selected = true;
    // $('.langSwitch1').trigger('change');
    $('.flag img').attr('src', langRootpath + 'h160/' + funLanguage + '.webp');
    $('.langSwitch1').next('.nice-select').find('.current').text(funLanguage.toUpperCase());
});