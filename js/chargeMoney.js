$(function(){
    var token = get_user_login();
    var currency=get_param('id')
    var name=get_param('name')
    $('.titles').html(name);
    function updateCode(addr){
        $('#code').html('');
        $('.address').html(addr);
        code(addr);
        copy(addr);
    }
    function getData(){
        layer_loading();
        var msg;
        $.ajax({
            type: "POST",
            url: _API + 'wallet/get_in_address',
            data: {
                currency:currency
            },
            dataType: "json",
            async: true,
            beforeSend: function beforeSend(request) {
                request.setRequestHeader("Authorization", token);
            },
            success: function(res){
                layer_close();
                if (res.type=="ok"){
                    console.log(res)
                    msg = res.message.split(',');
                    console.log(msg)
                    updateCode(currency == 3 ? msg[2] : msg[0]);
                } else {
                    layer_msg(res.message);
                }
            }
        })
        if(currency == 3){
            $('#switchChain').show();
            $('#switchChain button').click(function (e){
                let $e = $(e.target);
                $e.siblings().removeClass('active');
                $e.addClass('active');
                if(/trx/.test($e.attr('class'))){
                    updateCode(msg[2]);
                }else{
                    updateCode(msg[1]);
                }
            });
        }else{
            $('#switchChain').hide();
        }
    }
    function getrate(){
        $.ajax({
            type: "POST",
            url: _API + 'wallet/get_info',
            data: {
                currency:currency
            },
            dataType: "json",
            async: true,
            beforeSend: function beforeSend(request) {
                request.setRequestHeader("Authorization", token);
            },
            success: function(res){
                layer_close();
                if (res.type=="ok"){
                    console.log(res)
                    $('.mainnum').html(Number(res.message.min_number).toFixed(2))
                }else{
                    console.log(res.message)
                }
            }
        })
    }
    getData();
    getrate();
    function code(addre){

        $('#code').qrcode({
            width: 160,
            height:160,
            text:addre
        });
    }
    function copy(addre){
        var content = addre;
        var clipboard = new Clipboard('.copy', {
            text: function () {
                return content;
            }
        });
        clipboard.on('success', function (e) {
            layer_msg(CC('Copy Success'));
        });

        clipboard.on('error', function (e) {
            layer_msg(CC('Copy Error'))
        });
    }
})
