function CC(msg){
    let lang = localStorage.getItem('language') || navigator.language || navigator.userLanguage;
    let path;
    switch (lang.substring(0, 2)) {
        case "en":
           path = "en/";
            break;
        case "zh":
            path = "zh/";
        case "hk":
            path = "hk/";
            break;
        case "ko":
            path = "kr/";
            break;
        case "jp":
            path = "jp/";
            break;
        default:
            path = "en/";
            break;
    }
    let file = path + msg + '.js';
    $('body').append(`<script type="application/javascript" src="${file}"></script>`)
    return msg;

}

function appendLang(content){
    $('#myPolicy').html(content)
}