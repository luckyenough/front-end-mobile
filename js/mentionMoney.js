$(function () {
    var currency = get_param('id');
    var balance,min_number,rate;
    initDataToken({url:'user/info'},function(res){
        console.log(res);
        var userName = res.account_number;
        var countryCode = res.country_code;
        var urls = '';
        if(userName.indexOf('@')!=-1){
            urls = 'sms_mail';
        }else{
            urls = 'sms_send';
        }
        $('#fbtain').click(function(){
            initDatas({url:urls,type:'post',data:{
                    "user_string": userName,
                    "scene": 'mention_money',
                    "country_code":countryCode
                }},function(res){
                console.log(res);
                layer_msg(res.message)
                if(res.type=='ok'){
                    settime();
                }
            })
        })

    })
    if(currency == 3){
        $('#transferNetwork').show();
        $('#transferNetwork button').click(function (e){
            let $e = $(e.target);
            $e.siblings().removeClass('active');
            $e.addClass('active');
        });
    }else{
        $('#switchChain').hide();
    }
    initDataToken({url:'wallet/get_info',type:'post',data:{currency}},function(res){
        console.log(res)
        balance=res.change_balance;
        $('.balance-num').html(Number(balance).toFixed(8))
        $('.title').html(res.name)
        rate=res.rate;
        $('.ratenum').html(rate+'%')
        min_number=res.min_number;
        // $('.count').attr('placeholder','最小提币数量'+Number(min_number).toFixed(2))
        var num=0;
        $('.reach-num').html(num.toFixed(8));
    })
    function postData() {
        var address = $('.address').val();
        var number = $('.count').val();
        var password = $('.password').val();
        var codeText = $('.codes').val();
        var network = ''
        try{
            network = $('#transferNetwork button.active').attr('class').replace('active','').trim();
        }catch(ex){}
        if(!address){
            layer_msg('请输入提币地址');
            return;
        }
        if(!number){
            layer_msg('请输入提币数量');
            return;
        }
        if(!password){
            layer_msg('请输入资金密码');
            return;
        }

        if((number-0)<min_number){
            console.log(number,min_number)
            return layer_msg('输入的提币数量小于最小值');
        }
        if(!codeText){
            layer_msg(getlg('pyan'));
            return;
        }

        initDataToken({url:'wallet/out',type:'post',data:{currency,number,rate,address,password,sms_code:codeText,network}},function(res){
            console.log(res)
            layer_msg(res)
            setTimeout(() => {
                location.href='tradeAccount.html?id='+currency+'&type=2'
            }, 1500);
        })

    }
    $('.mention').click(function(){
        postData();
    })

    $('.all_in').click(function(){
        $('.count').val(balance)
    })
    $('.count').keyup(function(){
        var value=$(this).val();
        var num=value*(1-rate/100)
        $('.reach-num').html(num)
    })

})
var countdown = 60;
var generate_code = $("#mbtain,#fbtain");

function settime() {
    if (countdown == 0) {
        generate_code.attr("disabled", false);
        generate_code.val(tsend);
        $("#mbtain,#fbtain").css('color', '#5890bd');
        countdown = 60;
        return false;
    } else {
        $("#mbtain,#fbtain").attr("disabled", true);
        $("#mbtain,#fbtain").css('color', '#b6bfc4');
        generate_code.val( countdown + "s");
        countdown--;
    }
    setTimeout(function () {
        settime();
    }, 1000);
}