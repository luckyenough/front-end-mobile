"use strict"
!function(t,e){const n="We use cookies to analyze traffic and ads.",i="/data/flags",s="https://flagcdn.com",o=["afAfghanistan","axÅland Islands","alAlbania","dzAlgeria","asAmerican Samoa","adAndorra","aoAngola","aiAnguilla","aqAntarctica","agAntigua and Barbuda","arArgentina","amArmenia","awAruba","auAustralia","atAustria","azAzerbaijan","bsBahamas","bhBahrain","bdBangladesh","bbBarbados","byBelarus","beBelgium","bzBelize","bjBenin","bmBermuda","btBhutan","boBolivia","baBosnia and Herzegovina","bwBotswana","bvBouvet Island","brBrazil","ioBritish Indian Ocean Territory","bnBrunei","bgBulgaria","bfBurkina Faso","biBurundi","khCambodia","cmCameroon","caCanada","cvCape Verde","bqCaribbean Netherlands","kyCayman Islands","cfCentral African Republic","tdChad","clChile","cnChina","cxChristmas Island","ccCocos Islands","coColombia","kmComoros","cgRepublic of the Congo","cdDR Congo","ckCook Islands","crCosta Rica","ciCôte d'Ivoire","hrCroatia","cuCuba","cwCuraçao","cyCyprus","czCzechia","dkDenmark","djDjibouti","dmDominica","doDominican Republic","ecEcuador","egEgypt","svEl Salvador","gb-engEngland","gqEquatorial Guinea","erEritrea","eeEstonia","szEswatini","etEthiopia","fkFalkland Islands","foFaroe Islands","fjFiji","fiFinland","frFrance","gfFrench Guiana","pfFrench Polynesia","tfFrench Southern and Antarctic Lands","gaGabon","gmGambia","geGeorgia","deGermany","ghGhana","giGibraltar","grGreece","glGreenland","gdGrenada","gpGuadeloupe","guGuam","gtGuatemala","ggGuernsey","gnGuinea","gwGuinea-Bissau","gyGuyana","htHaiti","hmHeard Island and McDonald Islands","hnHonduras","hkHong Kong","huHungary","isIceland","inIndia","idIndonesia","irIran","iqIraq","ieIreland","imIsle of Man","ilIsrael","itItaly","jmJamaica","jpJapan","jeJersey","joJordan","kzKazakhstan","keKenya","kiKiribati","kpNorth Korea","krSouth Korea","xkKosovo","kwKuwait","kgKyrgyzstan","laLaos","lvLatvia","lbLebanon","lsLesotho","lrLiberia","lyLibya","liLiechtenstein","ltLithuania","luLuxembourg","moMacau","mgMadagascar","mwMalawi","myMalaysia","mvMaldives","mlMali","mtMalta","mhMarshall Islands","mqMartinique","mrMauritania","muMauritius","ytMayotte","mxMexico","fmMicronesia","mdMoldova","mcMonaco","mnMongolia","meMontenegro","msMontserrat","maMorocco","mzMozambique","mmMyanmar","naNamibia","nrNauru","npNepal","nlNetherlands","ncNew Caledonia","nzNew Zealand","niNicaragua","neNiger","ngNigeria","nuNiue","nfNorfolk Island","mkNorth Macedonia","gb-nirNorthern Ireland","mpNorthern Mariana Islands","noNorway","omOman","pkPakistan","pwPalau","psPalestine","paPanama","pgPapua New Guinea","pyParaguay","pePeru","phPhilippines","pnPitcairn Islands","plPoland","ptPortugal","prPuerto Rico","qaQatar","reRéunion","roRomania","ruRussia","rwRwanda","blSaint Barthélemy","shSaint Helena, Ascension and Tristan da Cunha","knSaint Kitts and Nevis","lcSaint Lucia","mfSaint Martin","pmSaint Pierre and Miquelon","vcSaint Vincent and the Grenadines","wsSamoa","smSan Marino","stSão Tomé and Príncipe","saSaudi Arabia","gb-sctScotland","snSenegal","rsSerbia","scSeychelles","slSierra Leone","sgSingapore","sxSint Maarten","skSlovakia","siSlovenia","sbSolomon Islands","soSomalia","zaSouth Africa","gsSouth Georgia","ssSouth Sudan","esSpain","lkSri Lanka","sdSudan","srSuriname","sjSvalbard and Jan Mayen","seSweden","chSwitzerland","sySyria","twTaiwan","tjTajikistan","tzTanzania","thThailand","tlTimor-Leste","tgTogo","tkTokelau","toTonga","ttTrinidad and Tobago","tnTunisia","trTurkey","tmTurkmenistan","tcTurks and Caicos Islands","tvTuvalu","ugUganda","uaUkraine","aeUnited Arab Emirates","gbUnited Kingdom","usUnited States","umUnited States Minor Outlying Islands","uyUruguay","uzUzbekistan","vuVanuatu","vaVatican City","veVenezuela","vnVietnam","vgBritish Virgin Islands","viUnited States Virgin Islands","gb-wlsWales","wfWallis and Futuna","ehWestern Sahara","yeYemen","zmZambia","zwZimbabwe"],a={"no":{"bv":1,"sj":1},"bv":{"no":1,"sj":1},"sj":{"no":1,"bv":1},"au":{"hm":1},"hm":{"au":1},"fr":{"mf":1},"mf":{"fr":1},"us":{"um":1},"um":{"us":1}},c="/asset-search.js?6f60cde2",r="/asset-markers.js?6f60cde2",l="/assets/package-leaflet.css?6f60cde2",d={"c-flags_sort_label":"View","c-flags_sort_column-flag":"Flag","c-flags_sort_column-country":"Country","c-flags_sort_column-population":"Population","c-flags_sort_column-area":"Total area","c-flags_sort_name-asc-grid":"by name, just flags","c-flags_sort_name-asc-table":"by name with basic information","c-flags_sort_population-desc-table":"by the most populous","c-flags_sort_population-asc-table":"by the least populous","c-flags_sort_area-desc-table":"by largest","c-flags_sort_area-asc-table":"by smallest"}
function u(t,e){return t.querySelector(e)}function p(t,e,n){const i=t.querySelectorAll(e)
return i.forEach(n),i}function f(){return"IntersectionObserver"in t&&"IntersectionObserverEntry"in t&&"isIntersecting"in t.IntersectionObserverEntry.prototype}function g(t){return e.createElement(t)}function h(t){return e.getElementById(t)}function m(t,n){const i=e.querySelectorAll(t)
return void 0!==n&&i.forEach(n),i}function b(t,e){t.appendChild(e)}function v(t){e.body.appendChild(t)}function y(t){t.style.display="block"}function w(t){t.style.display="none"}function L(t,e){t.innerHTML=e}function $(t,e){t.textContent=e}function C(t){let e,n,i=t.length
for(;--i;)n=Math.floor(Math.random()*(i+1)),e=t[n],t[n]=t[i],t[i]=e}function E(t){const e=g("script")
e.src=t,v(e)}function S(t){return void 0===d[t]?t:d[t]}function k(t){const e="-"===t.substring(2,3)?6:2
return{o:t,c:t.substring(0,e),n:t.substring(e)}}function x(t,e){t.addEventListener("click",e)}let N
function T(t){N=N||e.cookie
const n=N.match("(^|[^;]+)\\s*"+t+"\\s*=\\s*([^;]+)")
return n?n.pop():""}function M(){const t=T("country")
return 2==t.length?t.toLowerCase():"xx"}function q(){var i,s,o
m("[data-event]",(function(t){const e=t.getAttribute("data-event").split(",")
t.onclick=function(){ga("send","event",e[0],e[1],e[2])}})),m("#form-test",(function(t){t.querySelectorAll("input").forEach((function(t){t.value="flaglovers"})),t.style.display="none"})),i=e.body.classList,s=h("header"),e.addEventListener("click",(function(t){i.contains("nav-open")&&!s.contains(t.target)&&i.remove("nav-open")})),function(t,n){let i=!1
function s(){i||(E(c),i=!0)}t.contains(n+"-open")&&s(),h(n+"-toggler").addEventListener("click",s),h(n).addEventListener("click",(function(e){e.target===this&&t.remove(n+"-open")})),e.addEventListener("keyup",(function(e){27==e.keyCode?t.remove(n+"-open"):70==e.keyCode&&"INPUT"!=e.target.tagName&&"TEXTAREA"!=e.target.tagName&&h(n+"-toggler").click()}))}(e.body.classList,"search"),function(t,n,i){if(t||T("ccb"))return
const s=g("div")
s.id="cookies-alert",$(s,t)
const o=g("button")
$(o,"OK"),o.onclick=function(){w(s),function(t,n,i){const s=new Date
s.setTime(s.getTime()+1e3*(i||31536e3)),e.cookie=t+"="+n+"; expires="+s.toUTCString()+"; path=/"}("ccb","1")},s.appendChild(o),v(s)}(n),m("."+(o="next-element-")+"toggle",(function(t){const e=t.classList,n=t.nextElementSibling.classList
n.contains(o+"hidden")||n.add(o+"hidden"),e.add("a"),t.addEventListener("click",(function(){n.toggle("next-element-hidden-show"),e.toggle(o+"toggled")}))})),m("[data-clipboard]",(function(n){n.addEventListener("click",(function(){const i=g("textarea")
if(i.setAttribute("style","font-size:16px;width:1px;height:1px;"),this.parentNode.insertBefore(i,this.nextSibling),i.value=this.getAttribute("data-clipboard"),i.focus(),navigator.userAgent.match(/ipad|ipod|iphone/i)){i.contentEditable=!0,i.readOnly=!1
const n=e.createRange()
n.selectNodeContents(i)
const s=t.getSelection()
s.removeAllRanges(),s.addRange(n),i.setSelectionRange(0,999999)}else i.select()
e.execCommand("copy"),this.parentNode.removeChild(i),n.classList.contains("copied")||(n.classList.add("copied"),setTimeout((function(){n.classList.remove("copied")}),1500))}))})),function(t,e){const n=function(t,e){let n=t.getHours()
return n+=":",t.getMinutes()<10&&(n+="0"),n+=t.getMinutes(),e&&(n+=":",t.getSeconds()<10&&(n+="0"),n+=t.getSeconds()),n}
m("["+t+"utc]",(function(i){const s=new Date(i.getAttribute(t+"utc")+" UTC")
let o=""
switch(i.getAttribute(t+"format")){case e:o+=s.getDate()+". "+(s.getMonth()+1)+". "+s.getFullYear()+", ",o+=n(s,!1)
break
case e+"-short":o+=s.getDate()+". "+(s.getMonth()+1)+"., ",o+=n(s,!1)
break
default:o+=n(s,!0)}$(i,o)}))}("data-time-","datetime")}function A(){const t=(n="js-download",e.getElementById(n))
var n
if(!t||!t.dataset.code)return
let a=t.dataset.code,c=t.dataset.name
function r(t,e){return t||(t=".."),e||(e=".."),`${t}×${e} px`}function l(t,e){let n=arguments.length>2&&void 0!==arguments[2]?arguments[2]:0,i=`${"&nbsp;".repeat(n)}&lt;<span class="code-tag">${t}</span>`
if(e)for(let t in e){const s="string"==typeof e[t]?e[t].replace(/\n/g,"<br />"+"&nbsp;".repeat(2*(n+1)+2)):e[t]
s&&(i+=`<br />${"&nbsp;".repeat(2*(n+1))}<span class="code-attr">${t}</span>=&quot;<span class="code-value">${s}</span>&quot;`)}return i+="&gt;",i}function d(t,e,n){const i=[],o=[]
t.forEach((function(t){const e=`${s}/${t.cdn}/<em>${a}</em>.%s`+(t.density>1?` ${t.density}x`:"")
i.push(e.replace("%s","webp"==n?"png":n)),o.push(e.replace("%s","webp"))}))
let r,d=g("code")
if("webp"!=n)r=l("img",{src:`${s}/${e.cdn}/<em>${a}</em>.${n}`,srcset:i.slice(1).join(",\n"),width:e.width,height:e.height,alt:`<em>${c}</em>`})
else{const t=l("source",{type:"image/webp",srcset:o.join(",\n")},1),n=l("source",{type:"image/png",srcset:i.join(",\n")},1),d=l("img",{src:`${s}/${e.cdn}/<em>${a}</em>.png`,width:e.width,height:e.height,alt:`<em>${c}</em>`},1)
r=l("picture")+"<br />&nbsp;"+t+"<br />&nbsp;"+n+"<br />&nbsp;"+d+"<br />"+l("/picture")}return L(d,r),d}function f(){p(t,".sizes button:first-child",(function(t){t.click()}))}p(t,".preview",(function(n){const s=u(n,"em"),r=u(n,"button")
let l=null
function d(e,n,o){const r=g("button")
r.title=n,r.dataset.code=e,L(r,function(t,e){let n=i
"us-"==t.substring(0,3)&&(n=n.replace("/data/flags","/data/us"),t=t.substring(3))
const s=e/4*3
function o(){let t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:1
return`${n}/icon/${e*t}x${s*t}`}function a(t,e){let n=""
for(let i=1;i<=3;i++)n&&(n+=", "),n+=`${o(i)}/${t}.${e}`+(i>1?` ${i}x`:"")
return n}return`<picture><source type="image/webp" srcset="${a(t,"webp")}"><source type="image/png" srcset="${a(t,"png")}"><img src="${o()}/${t}.png" width="${e}" height="${s}" alt=""></picture>`}(e,28)),x(r,(function(){$(s,n),p(t,".example picture, .types picture",(function(t){let n=e,i=t.innerHTML.replace(new RegExp("/data/us/","g"),"/data/flags/")
o&&(n=n.substring(3),i=i.replace(new RegExp("/data/flags/","g"),"/data/us/")),L(t,i.replace(new RegExp("/[a-z]{2}(-[a-z]{2,3})?\\.","g"),"/"+n+".").replace(/width="\d+"/g,""))})),a=e,c=n,p(t,".item > .options .selected",(function(t){t.click()})),p(t,"[data-preview-code]",(function(t){$(t,e)})),p(t,"[data-preview-name]",(function(t){$(t,n)})),f()})),l.appendChild(r)}x(r,(function(){n.classList.toggle("on"),n.classList.contains("on")&&!l&&(l=g("div"),o.forEach((function(t){const e=k(t)
d(e.c,e.n,!1)})),n.dataset.us&&JSON.parse(n.dataset.us).forEach((function(t){d("us-"+t.substring(0,2),t.substring(2),!0)})),n.appendChild(l))})),x(e,(function(t){n.classList.contains("on")&&!r.contains(t.target)&&n.classList.remove("on")}))})),p(t,"[data-group-target]",(function(e){x(e,(function(){p(t,`[data-group-target=${e.dataset.groupTarget}]`,(function(t){t.classList.remove("selected")})),e.classList.add("selected"),p(t,`[data-group=${e.dataset.groupTarget}]`,(function(t){t.dataset.item==e.dataset.itemTarget?t.classList.remove("hidden"):t.classList.add("hidden")})),f()}))})),p(t,".sizes",(function(e){const n=e.dataset.previewTarget,i=e.dataset.previewFormatTarget,s=p(e,"button",(function(e){x(e,(function(){s.forEach((function(t){t.classList.remove("selected")})),e.classList.add("selected")
const o=JSON.parse(e.dataset.densities),a=o[0]
p(t,`[data-preview-size=${n}]`,(function(t){$(t,a.cdn)})),p(t,`[data-preview-html=${n}]`,(function(t){L(t,""),t.appendChild(d(o,a,i))}))}))}))})),p(t,".item",(function(t){const n=u(t,".options")
if(!n)return
let i,o
function l(t){i=JSON.parse(t.dataset.densities),o=i[0]}l(u(t,".option.selected"))
const f=p(n,".option",(function(e){x(e,(function(s){l(e)
const c=n.parentNode.querySelector(".example-fixed")
o.width>192?c.classList.add("example-fixed-wider"):c.classList.remove("example-fixed-wider"),function(t,e,n){const i=t.querySelector(e)
i&&n(i)}(t,".btn span",(function(t){$(t,r(o.width,o.height))})),p(c,"source, img",(function(t){const e=a.replace("us-","")
if("IMG"==t.tagName)t.src=`${o.dir}/${e}.png`,o.width?t.width=o.width:t.removeAttribute("width"),o.height?t.height=o.height:t.removeAttribute("height")
else{const n="image/png"==t.type?"png":"webp"
let s=""
i.forEach((function(t){s&&(s+=", "),s+=`${t.dir}/${e}.${n}`+(t.density>1?` ${t.density}x`:"")})),t.srcset=s}})),f.forEach((function(t){t.classList.remove("selected")})),e.classList.add("selected")}))}))
p(t,".btn",(function(t){const n=g("div")
var l,u
n.className="archives",u=n,(l=t).parentNode.insertBefore(u,l.nextSibling),x(t,(function(e){if(e.preventDefault(),n.classList.toggle("on"),!n.classList.contains("on"))return
L(n,"")
const l=g("div")
l.className="switchers",b(n,l),["PNG","WebP"].forEach((function(e){const u="PNG"==e?"":"-"+e.toLowerCase(),f="PNG"==e,h=g("button")
$(h,e)
const m=g("div")
m.classList.add("format"),f?h.classList.add("selected"):m.classList.add("hidden")
let v=""
var y
i.forEach((function(n){let i=t.dataset.single?`${s}/${n.cdn}/${a}.${e.toLowerCase()}`:`${s}/${n.cdn}${u}.zip`
v+=`<tr><td>${e}</td><td>${n.density}× density</td><td>${r(n.width,n.height)}</td><td><a href="${i}">download</a></td></tr>`})),L(m,"<strong>Download</strong><table>"+v+"</table>"),y="<strong>Embed via our free and fast Content Delivery Network</strong>",m.innerHTML+=y,m.appendChild(d(i,o,f?"png":"webp"))
const w=g("small")
L(w,'For other flags just replace <em>%s</em> with other <a href="https://flagcdn.com/en/codes.json" target="_blank">ISO 3166 country codes</a>.'.replace("%s",a).replace("%s",c)),m.appendChild(w),b(n,m),x(h,(function(){p(n,"button",(function(t){t.classList.remove("selected")})),h.classList.add("selected"),p(n,".format",(function(t){t.classList.add("hidden")})),m.classList.remove("hidden")})),b(l,h)}))})),x(e,(function(e){!n.classList.contains("on")||t.contains(e.target)||n.contains(e.target)||n.classList.remove("on")}))}))}))}function O(){m(".flag-tools",(function(n){const i=n.nextElementSibling
if(!i||"flag-grid"!==i.className)return void n.querySelectorAll("li").forEach((function(t){e.addEventListener("click",(function(e){t.classList.contains("on")&&!t.contains(e.target)&&t.classList.remove("on")}))}))
let s=null,o=null,a=[],c="tools",r={},l={sort:[function(t){if("grid"===t.view){if(!s)return
w(s),i.style.display="grid","replaceState"in history&&history.replaceState(null,null," ")}else{s||function(){const t=function(){const t=g("table")
t.className="flag-table"
const e=g("thead")
return t.appendChild(e),[["number","#",null,"table"],["flag","flag","name","grid"],["country sortable","country","name","table"],["value v-population sortable","population","population","table"],["value v-area sortable","area","area","table"]].forEach((function(t){const n=g("th")
n.scope="col",n.className=t[0]
const i=g("button")
$(i,"#"===t[1]?"#":S("c-flags_sort_column-"+t[1])),n.appendChild(i),e.appendChild(n)
const o=t[2],c=t[3]
p("sort",(function(t){n.classList.remove("asc"),n.classList.remove("desc"),t.attribute===o&&n.classList.add(t.direction)})),i.addEventListener("click",(function(){o||(s.classList.contains("reverse")?(s.classList.remove("reverse"),s.style.counterReset="table-counter 0"):(s.classList.add("reverse"),s.style.counterReset="table-counter "+(a.length+1)))
let t={attribute:o||r.sort.attribute,direction:"asc",view:c}
d("sort",t)&&(t.direction="desc"),u("sort",t)}))})),t}()
o=g("tbody"),t.appendChild(o)
let e=0
i.querySelectorAll("a").forEach((function(t){const n=t.getAttribute("data-population"),i=t.getAttribute("data-area"),s=/(<a[^>]+>)([\s\S]*<img[^>]+>)([\s\S]*<\/span>)([\s\S]*<\/a>)/m,o=g("tr"),c=g("td")
c.className="number",o.appendChild(c)
const r=g("td")
r.className="flag",r.innerHTML=t.outerHTML.replace(s,"$1$2$4").replace(/h160/g,"icon/72x54").replace(/h80/g,"icon/36x27").replace(/width="\d+"/g,'width="36"').replace(/height="\d+"/g,'height="27"'),o.appendChild(r)
const l=g("td")
l.className="country",l.innerHTML=t.outerHTML.replace(s,"$1$3$4"),o.appendChild(l)
const d=g("td")
d.className="value v-population",$(d,n.replace(/\B(?=(\d{3})+(?!\d))/g," ")),o.appendChild(d)
const u=g("td")
u.className="value v-area",$(u,i.replace(/\B(?=(\d{3})+(?!\d))/g," ")+" km")
const p=g("sup")
$(p,2),u.appendChild(p),o.appendChild(u),a.push({name:e++,population:parseInt(n),area:parseInt(i),el:o})})),p("sort",(function(e){"name"!=e.attribute?(t.classList.remove("w-population"),t.classList.remove("w-area"),t.classList.add("w-"+e.attribute)):-1==t.className.indexOf("w-")&&t.classList.add("w-population")})),s=t,i.parentNode.insertBefore(s,i)}(),w(i),s.style.display="table"
const e=t.attribute
a.sort((function(n,i){return(n[e]<i[e]?-1:n[e]>i[e]?1:0)*("asc"!=t.direction?-1:1)}))
for(let t=0;t<a.length;t++)o.appendChild(a[t].el)
"replaceState"in history&&history.replaceState(null,null,"#t")}}],color:[function(t){i.style.background=t}]}
function d(t,e){if(void 0===r[t])return!1
for(const n in r[t])if(void 0!==e[n]&&e[n]!==r[t][n])return!1
return!0}function u(t,e,n){const i=JSON.stringify(e)
n||ga("send","event","tools",t,i.replace(/[{}"]/g,"")),function(t,e){localStorage.setItem(t,e)}(c,i)
const s=r[t]?JSON.parse(JSON.stringify(r[t])):null
r[t]=JSON.parse(i)
for(let n=0;n<l[t].length;n++)l[t][n](e,s)}function p(t,e){l[t].push(e)}[{key:"sort",options:[["name-asc-grid",{attribute:"name",direction:"asc",view:"grid"}],["name-asc-table",{attribute:"name",direction:"asc",view:"table"}],["population-desc-table",{attribute:"population",direction:"desc",view:"table"},1],["population-asc-table",{attribute:"population",direction:"asc",view:"table"}],["area-desc-table",{attribute:"area",direction:"desc",view:"table"},1],["area-asc-table",{attribute:"area",direction:"asc",view:"table"}]]}].forEach((function(t){const i=g("li"),s=function(){const n=g("button")
i.appendChild(n),n.addEventListener("click",(function(){i.classList.toggle("on")})),e.addEventListener("click",(function(t){i.classList.contains("on")&&!i.contains(t.target)&&i.classList.remove("on")}))
const s=g("span")
$(s,S("c-flags_"+t.key+"_label")),n.appendChild(s)
const o=g("strong")
return n.appendChild(o),o}(),o=g("ul")
t.options.forEach((function(e,n){const a=g("li"),c=e[1],r=S("c-flags_"+t.key+"_"+e[0])
$(a,r),n||$(s,r),void 0!==e[2]&&(a.className="delimiter"),p(t.key,(function(){d(t.key,c)&&$(s,r),i.classList.remove("on")})),a.addEventListener("click",(function(){u(t.key,c)})),o.appendChild(a)})),i.appendChild(o),n.appendChild(i)}))
const f=(h=c,localStorage.getItem(h)||m)
var h,m
if(f&&f.match(/^{.*:"table".*}$/)){ga("send","event","tools","stored",f.replace(/[{}"]/g,""))
try{u("sort",JSON.parse(f),!0)}catch(t){}}else"#t"===t.location.hash&&(ga("send","event","tools","initial","table-view"),u("sort",{attribute:"name",direction:"asc",view:"table"},!0))}))}function I(){q(),function(){function t(t){return h("quiz-mini-"+t)}if(!t("image"))return
const e=t("image"),n=t("counter"),s=t("correct"),c=t("wrong"),r=t("options"),l=t("result"),d=t("result-correct"),u=t("result-wrong"),p=t("result-wrong-name"),f=t("refresh")
let m,b,v=[]
function L(){if(!v.length){if(v=o.slice(0),C(v),v.length<3)return
m=0,b=0,$(s,"0"),$(c,"0")}const f=k(v.shift())
let h,L,E,S=[f.o]
for(h=0;h<2;h++){do{if(E=o[Math.floor(Math.random()*o.length)],f.c in a&&k(E).c in a[f.c])E=null
else for(L=0;L<S.length;L++)S[L]==E&&(E=null)}while(!E)
S.push(E)}for(C(S),h=0;h<3;h++)!function(t,e){$(t,e.n),t.parentNode.onclick=function(){e.c==f.c?($(s,++m),y(d)):($(c,++b),$(p,f.n),y(u)),y(l),w(r),ga("send","event","mquiz",M(),f.c+"-"+e.c)}}(t("option-"+h),k(S[h]))
e.firstChild&&e.removeChild(e.firstChild),e.appendChild(function(t){const e=i+"/h240/"+f.c+".",n=i+"/h120/"+f.c+".",s=function(t){const i=g("source")
return i.type="image/"+t,i.srcset=e+t+" 2x, "+n+t+" 1x",i},o=g("img")
o.src=n+"png",o.height="120",o.alt="?",t&&(o.loading="lazy")
const a=g("picture")
return a.appendChild(s("webp")),a.appendChild(s("png")),a.appendChild(o),a}(m+b==0)),$(n,(o.length-v.length).toString()+"."),y(r),w(l),w(d),w(u)}L(),x(f,(function(){L()}))}(),O(),function(n,i,s,o,a){const c=h("map-header")
if(!c)return
const r=h("map"),l=h("map-wrapper")
let d=!1,u=function(){if(!d){t.innerWidth<=640&&(r.innerHTML='<div class="'+s+o+'-container"><div class="'+s+"top "+s+'right"><div class="'+s+o+"-zoom "+s+"bar "+s+o+'"><a id="map-close" class="'+s+o+'-zoom-in" href="#">×</a></div></div></div>',h("map-close").onclick=function(){return w(r),w(l),a.remove("map-open"),!1})
const c=g("link")
c.type="text/css",c.rel="stylesheet",c.href=i,e.head.appendChild(c),E(n),d=!0}}
if(c.onclick=function(){y(l),y(r),a.add("map-open"),u()},t.innerWidth>640)if(y(r),f()){const t=new IntersectionObserver((function(e){e[0].isIntersecting&&(u(),t.unobserve(e[0].target))}),{})
t.observe(l)}else u()}(r,l,"leaflet-","control",e.body.classList),function(){const t="quiz-big-"
function e(e){return h(t+e)}if(!e("image"))return
function n(t,e,n){const s=i+"/"+(e=e||"w")+2*(n=n||320)+"/"+t.c+".",o=i+"/"+e+n+"/"+t.c+".",a=function(t){const e=g("source")
return e.type="image/"+t,e.srcset=s+t+" 2x, "+o+t+" 1x",e},c=g("img")
c.src=o+"png","w"===e?c.width=n:c.height=n,c.alt="?"
const r=g("picture")
return r.appendChild(a("webp")),r.appendChild(a("png")),r.appendChild(c),r}const s=e("bar"),c=e("bar-num"),r=e("game-wrapper"),l=e("again"),d=e("overlay"),u=e("image"),p=e("counter-correct"),f=e("counter-wrong"),m=e("correct"),b=e("wrong"),v=e("prev")
let L,E,S=[],N={},T=[]
if(x(l,(function(){r.classList.remove("is-end"),q()})),r.dataset.codes){const t=JSON.parse(r.dataset.codes)
o.forEach((function(e){t.includes(k(e).c)&&S.push(e)}))}else S=o
function q(t){if(!T.length){if(S.forEach((function(t,e){N[t]=e})),T=S.slice(0),C(T),T.length<12)return
L=0,E=0,v.style.display="none",m.innerHTML="",b.innerHTML=""}const i=S.length-T.length+1
s.style.width=(i-1)/(S.length-1)*100+"%",$(c,i)
const o=k(T.shift())
let l,h,x,A=[o.o]
for(l=0;l<11;l++){do{if(x=S[Math.floor(Math.random()*S.length)],o.c in a&&k(x).c in a[o.c])x=null
else for(h=0;h<A.length;h++)A[h]==x&&(x=null)}while(!x)
A.push(x)}A.sort((function(t,e){return N[t]>N[e]?1:N[t]<N[e]?-1:0})),u.firstChild&&u.removeChild(u.firstChild),u.appendChild(t||n(o))
const O=T[0]?n(k(T[0])):null
let I=null,z=null
for(l=0;l<12;l++)!function(t,e,i){$(e,i.n),t.classList.remove("true"),t.classList.remove("false"),t.disabled=!1,i.c==o.c&&(I=t),t.onclick=function(){t.blur(),t.disabled=!0,i.c==o.c?(z||L++,t.classList.add("true"),y(d),setTimeout((function(){const t=g("span")
t.appendChild(n(o,"h",40))
const e=g("div")
$(e,o.n)
const i=g("div")
i.className="item",i.appendChild(t),i.appendChild(e)
const s=z?b:m
s.insertBefore(i,s.firstChild),$(p,L),$(f,E),v.style.display="flex",w(d),O?q(O):r.classList.add("is-end"),ga("send","event","bquiz",M(),o.c+"-"+(z?z.c:o.c))}),z?1250:375)):(z||(E++,z=i),t.classList.add("false"),y(d),setTimeout((function(){I.click()}),250))}}(e("option-"+l),e("option-text-"+l),k(A[l]))}q()}(),function(){const t=h("quiz")
if(!t||!t.classList.contains("play"))return
const e="quiz-play-"
let n=1
const i=h(e+"watch")
setInterval((function(){$(i,n++)}),1e3)
const s=t.querySelector('input[name="send"]')
w(s)
let o=1
const a=h(e+"status"),c=t.querySelectorAll(".flag")
c.forEach((function(t,e){e?w(t):y(t),t.querySelectorAll("input").forEach((function(e){e.classList.add("input-hide"),e.onchange=function(){w(t)
const e=t.nextElementSibling
e.classList.contains("flag")?y(e):(y(s),h("frm-quizForm").submit()),a.style.width=o++/c.length*100+"%"}}))}))}(),A()}function z(){const n="adsbygoogle",i="adsbygoogle-lazy"
function s(e){e.classList.remove(i),e.classList.add(n),(t.adsbygoogle=t.adsbygoogle||[]).push({})}let o=!1
function a(){o||(0!==t.location.host.indexOf("local.")&&E("https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9356627658540659"),o=!0)}e.querySelectorAll(`.${n}`).forEach((function(t){t.classList.remove(n),t.classList.add(i)}))
const c=e.querySelectorAll(`.${i}`)
if(f()){const t=new IntersectionObserver((function(e){e.forEach((function(e){if(e.isIntersecting){const n=e.target
s(n),t.unobserve(n),a()}}))}))
c.forEach((function(e){t.observe(e)}))}else c.forEach((function(t){s(t)})),a()
const r=e.querySelector(".frklm")
if(r){const e=new IntersectionObserver((function(n){n.forEach((function(n){if(n.isIntersecting){const i=n.target
e.unobserve(i),r.innerHTML='<div><ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-9356627658540659" data-ad-slot="9383587861" data-ad-format="auto" data-ad-channel="3197453460" data-full-width-responsive="false"></ins></div>',(t.adsbygoogle=t.adsbygoogle||[]).push({}),a()}}))}))
e.observe(r)}function l(n){const i=n.getBoundingClientRect()
return i.top>=0&&i.top<=(t.innerHeight||e.documentElement.clientHeight)}const d=e.head||e.getElementsByTagName("head")[0]
let u=0
function p(n,i){const s=e.createElement("li")
if(s.id=`rklm-${u}`,s.className="rklm",i){const t=Math.ceil((i+3*u)/3),n=Math.ceil((i+2*u)/2)
!function(t){const n=e.createElement("style")
n.textContent=t,d.appendChild(n)}(`\n\t\t\t\t#${s.id} {grid-row:${t} / ${t+1};}\n\t\t\t\t@media only screen and (max-width : 640px) {\n\t\t\t\t\t#${s.id}  {grid-row:${n} / ${n+1};}\n\t\t\t\t}\n\t\t\t`)}s.innerHTML='<ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-9356627658540659" data-ad-slot="9383587861" data-ad-format="auto" data-ad-channel="8248143656" data-full-width-responsive="false"></ins>',n.appendChild(s),(t.adsbygoogle=t.adsbygoogle||[]).push({}),a(),u++}function g(t,e){const n=new IntersectionObserver((function(t){t.forEach((function(t){if(t.isIntersecting){const i=t.target
n.unobserve(i),p(i.parentNode,e)}}))}))
n.observe(t)}const h=e.querySelector(".flag-grid")
if(h&&"none"!==h.style.display){const n=h.querySelectorAll("li")
if(n.length<4)return
const i=n[0]
let s=null,o=0
if(l(i)?n.forEach((function(t){s||(o++,l(t)||(s=t))})):function(n){const i=n.getBoundingClientRect()
return i.top>=0&&i.top>=(t.innerHeight||e.documentElement.clientHeight)}(i)&&(s=i),s){g(s,o)
let t=0,e=null
n.forEach((function(n){t++,n===s?e=0:null===e||!e++||e%42||g(n,t)}))}}}"loading"!==e.readyState?I():e.addEventListener("DOMContentLoaded",I),t.addEventListener("load",(function(){setTimeout((function(){z()}),125),function(t,e){function n(e){const n=g("img")
n.src="https://toplist."+e,n.width=1,n.height=1,n.alt="TOPlist",t.appendChild(n)}n("cz/dot.asp?id=716967"),n("sk/dot.asp?id=1062564"),t.className="hide",v(t)}(g("div"))}))}(window,document)
